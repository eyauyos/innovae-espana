using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTwoDirectionalMovement : MonoBehaviour
{
    public Transform _refFpsCamera;
    public float speed = 5;

    [Header("Running")]
    public bool canRun = true;
    public bool IsRunning { get; private set; }
    public float runSpeed = 9;
    public KeyCode runningKey = KeyCode.LeftShift;

    Rigidbody rigidbody;
    /// <summary> Functions to override movement speed. Will use the last added override. </summary>
    //public List<System.Func<float>> speedOverrides = new List<System.Func<float>>();



    void Awake()
    {
        // Get the rigidbody on this.
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // Update IsRunning from input.
        IsRunning = canRun && Input.GetKey(runningKey);

        // Get targetMovingSpeed.
        float targetMovingSpeed = IsRunning ? runSpeed : speed;
        // if (speedOverrides.Count > 0)
        // {
        //     targetMovingSpeed = speedOverrides[speedOverrides.Count - 1]();
        // }

        // Get targetVelocity from input.
        //Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"),0).normalized*targetMovingSpeed;
        Vector3 _direction = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0).normalized;
        // Apply movement.
        //Vector3 _direction = new Vector3(_refFpsCamera.forward.x + _refFpsCamera.forward.x, 0, _refFpsCamera.forward.z).normalized;
        // rigidbody.velocity = transform.rotation * new Vector3(targetVelocity.x, rigidbody.velocity.y, targetVelocity.y);

        rigidbody.velocity =  (_refFpsCamera.forward*_direction.y + _refFpsCamera.right*_direction.x).normalized*targetMovingSpeed;//new Vector3(targetVelocity.x*_direction.x, rigidbody.velocity.y, targetVelocity.y*_direction.y);
    }
}
